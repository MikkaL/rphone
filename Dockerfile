FROM node:8
WORKDIR /rphone
COPY package.json .
RUN npm install
COPY . ./
EXPOSE 3030
CMD [ “npm”, “start” ] 