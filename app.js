var createError = require("http-errors"),
    express = require("express"),
    path = require("path"),
    cookieParser = require("cookie-parser"),
    bodyParser = require("body-parser"),
    logger = require("morgan"),
    conver = require("xml-js"),
    indexRouter = require("./routes/index"),
    popRouter = require("./routes/pop"),
    app = express();
app.use(bodyParser.urlencoded({ extended: !1 }));
app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");
app.use(express.json());
app.use(express.urlencoded({ extended: !1 }));
app.use(cookieParser());
app.use(express["static"](path.join(__dirname, "./public")));
app.use(express["static"](path.join(__dirname, "./views")));
app.use(express["static"](path.join(__dirname, "./")));
app.use("/", indexRouter);
app.use("/index", indexRouter);
app.use("/pop", popRouter);
module.exports = app;