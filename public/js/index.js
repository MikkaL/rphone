$('#fileselect').on('change', function() {
    //Reference the FileUpload element.
    var fileUpload = document.getElementById("fileselect");
    //Validate whether File is valid Excel file.
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
    if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof(FileReader) != "undefined") {
            var reader = new FileReader();

            //For Browsers other than IE.
            if (reader.readAsBinaryString) {
                reader.onload = function(e) {
                    ProcessExcel(e.target.result);
                };
                reader.readAsBinaryString(fileUpload.files[0]);
            } else {
                //For IE Browser.
                reader.onload = function(e) {
                    var data = "";
                    var bytes = new Uint8Array(e.target.result);
                    for (var i = 0; i < bytes.byteLength; i++) {
                        data += String.fromCharCode(bytes[i]);
                    }
                    ProcessExcel(data);
                };
                reader.readAsArrayBuffer(fileUpload.files[0]);
            }
        } else {
            alert("Por problemas en leer el archivo prueba cambiando de navegador \r\n Se recomienda Google Chrome \r\n https://www.google.com/chrome/");
        }
    } else {
        alert("Por favor ingresa un archivo Excel valido");
    }
});

function checklogin() {
    var cook = Cookies.get('sessionlog');
    if (cook != undefined) {
        document.getElementById("pop-up").style.visibility = 'hidden';
    } else {
        document.getElementById("pop-up").innerHTML = '<div class="log-in-pop">'+
            '<div class="tablecontainer">'+
                '<table>'+
                    '<tr>'+
                        '<td class="log-in-cell">RUT</td>'+
                        '<td>'+
                            '<input type="text" name="user" id="rut"/>'+
                        '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td colspan="2">'+
                            '<p id="userNotice"></p>'+
                        '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td class="log-in-cell">Contrase&#241;a</td>'+
                        '<td>'+
                            '<input type="password" name="password" id="password">'+
                        '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td colspan="2">'+
                            '<p id="errorNotice"></p>'+
                        '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td colspan="2">'+
                            '<input type="button" onclick="log_in();" class="button_pop" name="ingresar" value="ingresar">'+
                        '</td>'+
                    '</tr>'+
                '</table>'+
            '</div>'+
        '</div>';
        document.documentElement.style.overflow = 'hidden';
    }
}
$('#rut').on('change', function() {
    if (!valida_Rut($(this).val())) {
        document.getElementById('userNotice').innerHTML = 'Rut Invalido';
    }
});

function log_in() {
    var rut = formateaRut($('#rut').val());
    var password = $('#password').val();
    if (valida_Rut(rut) &&
        password.length > 0) {
        $.ajax({
            type: "GET",
            url: "/validate_user_call",
            data: {
                user: rut,
                password: password
            },
            error: (err) => {
                const err_split = err.responseText.split("<pre>");
                const error = err_split[1].split("</pre>")[0];
                alert(error);
            },
            success: (info) => {
                callback(info)
            }
        });
    } else {
        document.getElementById('errorNotice').innerHTML = 'RUT o Contrase&#241;a Inválido';
    }
}
function callback(info) {
    if(info.detail == undefined){
        var rut = formateaRut($('#rut').val());
        var password = $('#password').val();
        Cookies.set('sessionlog', btoa(rut+':'+password))
        checklogin();
    } else {
        document.getElementById('errorNotice').innerHTML = 'RUT o Contrase&#241;a Inválido';   
    }
}
function ProcessExcel(data) {
    //Read the Excel File data.
    var workbook = XLSX.read(data, {
        type: 'binary'
    });
    //Fetch the name of First Sheet.
    var firstSheet = workbook.SheetNames[0];

    //Read all rows from First Sheet into an JSON array.
    var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    var cellphone = [];
    var temp = []
    for (var i = 0; i < excelRows.length; i++) {

        if (excelRows[i].NOMBRE != undefined) {
            if (temp[0] == excelRows[i].NOMBRE) {
                temp.push(excelRows[i].NOMBRE);
                temp.push(excelRows[i].SKU);
            } else {
                if (temp.length > 0) {
                    cellphone.push(temp);
                    temp = []
                }
                temp.push(excelRows[i].NOMBRE);
                temp.push(excelRows[i].SKU);
            }
        } else {
            temp.push(excelRows[i].SKU);
        }
        if ((i+1) == excelRows.length) {
            cellphone.push(temp);
        }
    }
    var text = ''
    for (i in cellphone) {
        for (e in cellphone[i]) {
            text += cellphone[i][e] + '\n'
        }
    }
    $('#sku').val(text)
};

function create_pop() {
    var sucursal = $('#sucursal').val();
    var output = $('#sku').val();
    $.ajax({
        type: "POST",
        url: "/pop",
        data: {
            sku: output, 
            sucursal: sucursal
        },
        error: (err) => {
            const err_split = err.responseText.split("<pre>");
            const error = err_split[1].split("</pre>")[0];
            alert(error);
        },
        success: (data) => {
            $('#content').html(data);
            window.open('../../views/pop.html');
        }
    });
}
function formateaRut(rut) {
 
    var actual = rut.replace(/^0+/, "");
    if (actual != '' && actual.length > 1) {
        var sinPuntos = actual.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        for (i = inicio.length - 1; i >= 0; i--) {
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
}

function valida_Rut(rut) {
    var tmpstr = "";
    var intlargo = rut
    if (intlargo == undefined) {
        return false;
    }
    if (intlargo.length > 0) {
        crut = rut
        largo = crut.length;
        if (largo < 2) {
            return false;
        }
        for (i = 0; i < crut.length; i++)
            if (crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-') {
                tmpstr = tmpstr + crut.charAt(i);
            }
        rut = tmpstr;
        crut = tmpstr;
        largo = crut.length;

        if (largo > 2)
            rut = crut.substring(0, largo - 1);
        else
            rut = crut.charAt(0);

        dv = crut.charAt(largo - 1);

        if (rut == null || dv == null)
            return 0;

        var dvr = '0';
        suma = 0;
        mul = 2;

        for (i = rut.length - 1; i >= 0; i--) {
            suma = suma + rut.charAt(i) * mul;
            if (mul == 7)
                mul = 2;
            else
                mul++;
        }

        res = suma % 11;
        if (res == 1)
            dvr = 'k';
        else if (res == 0)
            dvr = '0';
        else {
            dvi = 11 - res;
            dvr = dvi + "";
        }

        if (dvr != dv.toLowerCase()) {
            return false;
        }
        return true;
    }
}