const api = require('../api/index.js');
var convert = require('xml-js');
var xml2js = require('xml2js');
var xml2json = require('xml2json');
var inspect = require('eyes').inspector({ maxLength: false })
var parseString = xml2js.parseString;

function look_up(sku) {
    return new Promise((resolve, reject) => {
        api.make_API_call(sku)
            .then((res) => {
                resolve(res);
            })
            .catch(error => {
                reject(error);
            })
    });
}

function look_up_plan(sku, cookie) {
    return new Promise((resolve, reject) => {
        api.make_API_call_staging(sku, cookie)
            .then((res) => {
                resolve(res);
            })
            .catch(error => {
                reject(error);
            })
    });
}

function look_up_prices(sku, sucursal) {
    return new Promise((resolve, reject) => {
        api.make_API_call_productos(sku, sucursal)
            .then((response) => {
                var xml = createXml(response)
                api.make_API_call_MPromo(xml)
                    .then((res) => {
                        resolve(parseResponse(res.substring(5, res.length), sku, response.precioMaster,response.precioVigente))
                    })
                    .catch(error => {
                        reject(error);
                    })
            })
            .catch(error => {
                reject(error);
            })
    });
}

function createXml(response) {
    try {
        const products = [];
        let body = response
        products.push({
            _attributes: {
                id: '1',
                cant: '1',
                upcimp: body.ean13,
                prod: body.ean13,
                descr: body.descripcion,
                division: body.division,
                area: body.area,
                depto: body.deptop,
                linea: body.linea,
                slinea: body.subLinea,
                evento: body.evento,
                prioridad: body.prioridad,
                sprioridad: body.subPrioridad,
                precio: body.precioVigente,
                precio1: body.precioVigente,
                estilo: body.ean13,
                marca_prod: body.marcaProd,
                marca_propia: body.marcaPropia,
                marca_prov: body.marcaPrv,
                prov_act: '00000000000',
                prov_ant: '00000000000',
                bdesc: '0',
                tipoart: body.tipoArticulo,
                proced: body.procedencia,
                rangoprecio: body.ranPrecio,
                tempo: body.temporada,
                concesion: body.concesion,
                fletedesp: body.fleteDbo,
                tapiceria: body.tapiceria,
                ciclovida: body.cicloVida,
                indus: body.industrializado,
                origdesp: body.oreDespachoBt,
                prodinternet: body.prdInternet,
                tiponego: body.tipNegociacion,
            },
        });
        const xml = {
            _declaration: {
                _attributes: {
                    version: '1.0',
                    encoding: 'utf-8',
                },
            },
            pangui: {
                O: {
                    _attributes: {
                        oper: 'C',
                    },
                    T: {
                        _attributes: {
                            fecha: format(new Date()),
                            tienda: '12',
                            pos: 'Manu',
                            ntrans: '99999999',
                            tipo_trans: '1',
                            comercio: '1',
                            rpuntos: '0',
                            total: '7990',
                        },
                        C: {
                            _attributes: {
                                cliente: 'rmisurut',
                                tcliente: '',
                                tevento: '',
                            },
                        },
                        PS: {
                            P: {
                                _attributes: {
                                    tpago: '1',
                                    moneda: 'P',
                                    ncuotas: '0',
                                    tcuotas: '0',
                                    monto: '7990',
                                },
                            },
                        },
                        LS: {
                            L: [
                                ...products,
                            ],
                        },
                    },
                },
            },
        };
        return convert.json2xml(xml, { compact: true });
    } catch (e) {
        return e;
    }
};


function format(date) {
    var month = date.getMonth() + 1; // getMonth() is zero-based
    var day = date.getDate();
    var year = ("" + date.getFullYear()).substring(2, date.getFullYear().length);
    var hour = date.getHours();
    var minutes = date.getMinutes();

    return [(day > 9 ? '' : '0') + day,
        (month > 9 ? '' : '0') + month,
        year,
        (hour > 9 ? '' : '0') + hour,
        (minutes > 9 ? '' : '0') + minutes
    ].join('');
};

function parseResponse(xml, sku, master, vigente) {
    var parsed = convert.xml2js(xml).elements[0].elements
    var newJson = '{'+
    '"precioMaster": ' + parseInt(master, 10) + ','+
    '"precioVigente": ' + parseInt(vigente, 10) + ','+
    '"promoPrices": [';
    for (i in parsed) {
        newJson += '{"tr": "' + parsed[i].attributes.tr + '",';
        newJson += '"haypromo": "' + parsed[i].attributes.hay_promo + '",';
        for (e in parsed[i].elements) {
            var parsedSku = parsed[i].elements[e].elements[0].elements[0].attributes.prod.substring(1, parsed[i].elements[e].elements[0].elements[0].attributes.prod.length)
            if (parsedSku == sku) {
                newJson += '"precio": ' + parsed[i].elements[e].elements[0].elements[0].attributes.precio + ','
                newJson += '"prod": "' + parsedSku +'",'
                newJson += '"mdesc": ';
                mdesc = parsed[i].elements[e].elements[0].elements[0].attributes.mdesc;
                newJson += (mdesc != undefined)? mdesc + '},' : '0},'
            }
        }
    }

    newJson = (newJson.substring(0, newJson.length-1)+']}')
    return JSON.parse(newJson)
}

module.exports = {
    look_up: look_up,
    look_up_plan: look_up_plan,
    look_up_prices: look_up_prices
}