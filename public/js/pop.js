var jsons = [];
var isArray = false;
var cellArray = []
var lastSku = '';
var length;
var jsonlength = 0;
var idglob = 0;

function printPOST() {
    if (Cookies.get("sku") == undefined) {
        window.close()
    }
    cellArray = arraylify(Cookies.get("sku"));
    length = findlength(cellArray);
    lastSku = cellArray[cellArray.length - 1][cellArray[cellArray.length - 1].length - 1];
    isArray = Array.isArray(cellArray[0]);
    for (i in cellArray) {
        if (isArray) {
            for (e in cellArray[i]) {
                if (e != 0) {
                    call_data(cellArray[i][e], callback, cellArray[i]);
                }
            }
        } else {
            call_data(cellArray[i], callback);
        }
    }
    // Cookies.remove('sucursal')
    // Cookies.remove('sku')
};

function call_data(sku, cb, array) {
    $.ajax({
        type: "GET",
        url: "/pop/look_up",
        data: {
            id: sku
        },
        error: (err) => {
            const err_split = err.responseText.split("<pre>");
            const error = err_split[1].split("</pre>")[0];
            alert(error);
        },
        success: (info) => {
            cb(info)
        }
    });
    if (sku == search_last_sku(sku)) {
        for (i in array) {
            if (i != 0) {
                call_plans(array[i], callback_planes, array)
            }
        }
    }
};

function call_plans(sku, cb, array) {
    var buffer = Cookies.get('sessionlog');
    $.ajax({
        type: "GET",
        url: "/pop/look_up_plans",
        data: {
            id: sku,
            cookie: buffer
        },
        error: (err) => {
            const err_split = err.responseText.split("<pre>");
            const error = err_split[1].split("</pre>")[0];
            alert(error);
        },
        success: (info) => {
            cb(info, sku);
        }
    });
    if (sku == search_last_sku(sku)) {
        for (i in array) {
            if (i != 0) {
                call_price(array[i], callback_price)
            }
        }
    }
};

function call_price(sku, cb) {
    var sucursal = Cookies.get('sucursal');
    $.ajax({
        type: "GET",
        url: "/pop/look_up_price",
        data: {
            id: sku,
            sucursal: sucursal
        },
        error: (err) => {
            const err_split = err.responseText.split("<pre>");
            const error = err_split[1].split("</pre>")[0];
            alert(error);
        },
        success: (info) => {
            cb(info, sku)
        }
    });
};


function callback(info) {
    var newJson = JSON.stringify(info);
    var work = JSON.parse(newJson);
    if (work.error == undefined) {
        var proceIden;
        var camaIden;
        var pantIden;
        var colorIden;
        var operadorIden;

        for (i in work.attributes) {
            if (work.attributes[i].identifier == 'mod_procesador') {
                proceIden = i
            }
            if (work.attributes[i].identifier == 'tipo_procesador') {
                proceIden = i
            }
            if (work.attributes[i].identifier == 'camara_mp') {
                camaIden = i
            }
            if (work.attributes[i].identifier == 'pantalla_pulgadas') {
                pantIden = i
            }
            if (work.attributes[i].name.toLowerCase() == 'color') {
                colorIden = i
            }
            if (work.attributes[i].identifier == 'operador') {
                operadorIden = i
            }
        }
        var sku = work.partNumber;
        var nombre = work.name;
        var procesador = '';
        var camara = '';
        var pantalla = '';
        var color = '';
        var operador = 'liberado';

        if (proceIden != undefined) procesador = work.attributes[proceIden].value;
        if (camaIden != undefined) camara = work.attributes[camaIden].value;
        if (pantIden != undefined) pantalla = work.attributes[pantIden].value;
        if (operadorIden != undefined) operador = work.attributes[operadorIden].value.toLowerCase();
        if (colorIden != undefined) {
            if (work.colors == undefined) {
                color += work.attributes[colorIden].value
            }
            if (work.attributes[colorIden].value == undefined) {
                for (i in work.colors) {
                    color += work.color[i]
                }
            }
        }

        var preciolista = work.prices.formattedListPrice;
        var preciooferta = work.prices.formattedOfferPrice;
        var tarjetaripley = false;

        if (parseInt(work.prices.cardPrice) <= parseInt(work.prices.offerPrice)) {
            preciooferta = work.prices.formattedCardPrice;
            tarjetaripley = true;
        }
        if (isArray) {
            nombre = search_name_by_sku(sku);
            fillJsons(sku, nombre, procesador, camara, pantalla,
                color, operador, preciolista, preciooferta,
                tarjetaripley);
        } else {
            fillJsons(sku, nombre, procesador, camara, pantalla,
                color, operador, preciolista, preciooferta,
                tarjetaripley);
        }
        if (sku == search_last_sku(sku)) {
            search_position_by_name

            document.getElementById('body').innerHTML += fillHTMLCaracteristicas(jsons[i])
        }
    } else {
        jsons.push('');
    }
};
function callback_planes(info, sku) {
    var newJson = JSON.stringify(info);
    var work = JSON.parse(newJson);
    var id = search_position_by_name(search_name_by_sku(sku));
    var temp = jsons[id];
    if (work != '') {
        for (i in work) {
                var operador = work[i].plan_operator.toLowerCase()
                var skuid = temp.id;
                var html = '' +
                    '<table style="margin:auto">' +
                        '<tr>' +
                            '<td class="plan-detail">' +
                                'Valor Inicial' +
                            '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="plan-price">' +
                                '$' + formatPrice(work[i].device_price) +
                            '</td>' +
                        '</tr>' +
                    '<tr>' +
                    '<td class="plan-detail">' +
                    'con un plan de $' + formatPrice(work[i].plan_monthly_cost);
                if (work[i].is_portability) {
                    html += ' P*';
                    var disclaimerid = 'disclaimer' + skuid
                    document.getElementById(disclaimerid).style.visibility = 'visible';

                }
                html += '</td>' +
                    '</tr>' +
                    '</table>';
                var tableidplan = operador + 'Plan' + skuid;
                var tablecardplan = operador + 'Plancard' + skuid;
                var planesbar = 'planesbar' + skuid
                document.getElementById(planesbar).rowSpan = '5';
                document.getElementById(planesbar).style.visibility = 'visible';
                document.getElementById(planesbar).style.background = '#646464';
                document.getElementById(tableidplan).innerHTML = html;
                document.getElementById(tablecardplan).innerHTML = '<img class="img-logo" src="../../public/images/card.svg">'
            
        }
    }
};

function callback_price(info, sku) {
    var newJson = JSON.stringify(info);
    var work = JSON.parse(newJson);
    var id = search_position_by_name(search_name_by_sku(sku));
    var json = jsons[id]
    var operador = 'nada';
    var operadores = ['claro', 'entel', 'wom', 'movistar', 'liberado']
    if (json.claro.sku.length > 0) {
        for (c in json.claro.sku) {
            if (json.claro.sku[c].trim() == sku) operador = 'claro'
        }
    }
    if (json.entel.sku.length > 0) {
        for (c in json.entel.sku) {
            if (json.entel.sku[c].trim() == sku) operador = 'entel'
        }
    }
    if (json.wom.sku.length > 0) {
        for (c in json.wom.sku) {
            if (json.wom.sku[c].trim() == sku) operador = 'wom'
        }
    }
    if (json.movistar.sku.length > 0) {
        for (c in json.movistar.sku) {
            if (json.movistar.sku[c].trim() == sku) operador = 'movistar'
        }
    }
    if (json.liberado.sku.length > 0) {
        for (c in json.liberado.sku) {
            if (json.liberado.sku[c].trim() == sku) operador = 'liberado'
        }
    }
    
    var idprecion = operador + 'PrecioN' + json.id;
    var idpreciop = operador + 'PrecioOP' + json.id;
    var ripleycard = operador + 'RipleyCard' + json.id;
    var precioLista = document.getElementById(idprecion).innerHTML.replace(/[^0-9.]/g, "");
    var precioOferta = document.getElementById(idpreciop).innerHTML.replace(/[^0-9.]/g, "");

    precioOferta = (precioOferta == NaN) ? 0 : precioOferta
    precioLista = (precioLista == NaN) ? 0 : precioLista

    if (operador != 'nada') {
        if (precioLista == 0) {
            document.getElementById(idprecion).innerHTML = '$' + formatPrice(info.precioMaster);
        }
        if (precioLista > info.precioMaster) {
            document.getElementById(idprecion).innerHTML = '$' + formatPrice(info.precioMaster);
        }
        for (i in info.promoPrices) {
            var iscard = (info.promoPrices[0].tr == 'S') ? true : false;
            var preciodeoferta = info.promoPrices[0].precio - info.promoPrices[0].mdesc
            if (precioOferta == 0) {
                document.getElementById(idpreciop).innerHTML = '$' + formatPrice(preciodeoferta);
            }
            if (precioOferta > preciodeoferta) {
                document.getElementById(idpreciop).innerHTML = '$' + formatPrice(preciodeoferta);
                if (iscard) {
                    document.getElementById(ripleycard).style.visibility = 'visible'
                }else{
                    document.getElementById(ripleycard).style.visibility = 'hidden'
                }
            }
        }
        document.getElementById('container' + json.id).style.visibility = 'visible'
    }
};

function fillJsons(sku, nombre, procesador, camara, pantalla, color, operador,
    preciolista, preciooferta, tarjetaripley) {
    operator = ''
    var json = search_name(nombre);
    if (json == null) {
        json =  '{' +
                    '"id": '+ idglob + ',' +
                    '"nombre" : "' + nombre.replace(/"/g, '\\"') + '",' +
                    '"procesador" : "' + procesador.replace(/"/g, '\\"') + '",' +
                    '"camara" : "' + camara.replace(/"/g, '\\"') + '",' +
                    '"pantalla" : "' + pantalla.replace(/"/g, '\\"') + '",' +
                    '"color" :' +
                    '["' + color.replace(/"/g, '\\"') + '"],' +
                    '"claro": {' +
                        '"sku": [],'+
                        '"precioN": "",' +
                        '"precioOP": "",' +
                        '"ripleyCard": false' +
                    '},' +
                    '"entel": {' +
                        '"sku": [],'+
                        '"precioN" : "",' +
                        '"precioOP" : "",' +
                        '"ripleyCard" : false' +
                    '},' +
                    '"movistar": {' +
                        '"sku": [],'+
                        '"precioN" : "",' +
                        '"precioOP" : "",' +
                        '"ripleyCard" : false' +
                    '},' +
                    '"wom": {' +
                        '"sku": [],'+
                        '"precioN" : "",' +
                        '"precioOP" : "",' +
                        '"ripleyCard" : false' +
                    '},' +
                    '"liberado": {' +
                        '"sku": [],'+
                        '"precioN" : "",' +
                        '"precioOP" : ""' +
                    '}' +
                '}';
        var jsonSku = JSON.parse(json);
        jsonSku[operador]['sku'].push(sku);
        jsonSku[operador]['precioOP'] = preciooferta;
        jsonSku[operador]['ripleyCard'] = tarjetaripley;
        jsons.push(jsonSku)
        idglob++
    } else {
        var exists = false
        for (i in json.color) {
            if (json.color[i].toLowerCase() == color.toLowerCase()) exists = true
        }
        if (!exists) {json.color.push(color)}
        json[operador]['sku'].push(sku);
        json[operador]['precioN'] = preciolista;
        json[operador]['precioOP'] = preciooferta;
        json[operador]['ripleyCard'] = tarjetaripley;
    }
};

function fillHTMLCaracteristicas(sku) {
    var colores = '';
    var pantalla = '';

    if (sku.color.length > 1) {
        for (i in sku.color) {
            if (sku.color[i].length > 0) {
                colores += ' ' + sku.color[i] + ' |';
            }
        }
        if (colores[colores.length - 1] == '|') {
            colores = colores.substring(0, colores.length - 2)
        }
    } else {
        colores = sku.color[0];
    }
    if (sku.pantalla.indexOf('"') == -1 && sku.pantalla.length > 0) pantalla = sku.pantalla + '"';
    var html = '' +
        '<div class="container-pop" id="container' + sku.id + '">' +
            '<div id="popheader" class="box-container sbtitle">' +
                '<div id="nombre" class="box-title sb2">' + sku.nombre + '</div>' +
                '<table class="table-pop">' +
                    '<tr>' +
                        '<td class="charac">' +
                            '<div class="box sb1">' +
                                '<p class="caract"> PROCESADOR </p>' +
                            '</div>' +
                        '</td>' +
                        '<td>' +
                            '<div class="box-b">' +
                                '<p id="procesador" class="desc">' + sku.procesador + '</p>' +
                            '</div>' +
                        '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="charac">' +
                            '<div class="box sb1">' +
                                '<p class="caract">CAMARAS</p>' +
                            '</div>' +
                        '</td>' +
                        '<td>' +
                            '<div class="box-b">' +
                                '<p id="camara" class="desc">' + sku.camara + '</p>' +
                            '</div>' +
                        '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="charac">' +
                            '<div class="box sb1">' +
                                '<p class="caract">PANTALLA</p>' +
                            '</div>' +
                        '</td>' +
                        '<td>' +
                            '<div class="box-b">' +
                                '<p id="pantalla" class="desc">' + pantalla + '</p>' +
                            '</div>' +
                        '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="charac">' +
                            '<div class="box sb1">' +
                                '<p class="caract">COLORES</p>' +
                            '</div>' +
                        '</td>' +
                        '<td>' +
                            '<div class="box-b">' +
                                '<p id="color" class="desc">' + colores + '</p>' +
                            '</div>' +
                        '</td>' +
                    '</tr>' +
                '</table>' +
            '</div>' +
            '<div id="popprice" class="pop-content-price">' +
                '<table class="table-phone">' +
                    '<tr>' +
                        '<td class="logos-header"></td>' +
                        '<td class="table-pop-header">' +
                            'PRECIO NORMAL PREPAGO' +
                        '</td>' +
                        '<td class="table-pop-header">' +
                            'PRECIO OFERTA PREPAGO' +
                        '</td>' +
                        '<td class="notif-tarjeta"></td>' +
                        '<td id="planesbar' + sku.id + '" class="planes-bar" rowspan="1" style="visibility:hidden;">' +
                            '<div class="rotated">' +
                                'PLANES' +
                            '</div>' +
                        '</td>' +
                        '<td id="valor-plan" class="table-pop-header">' +
                            'VALOR EQUIPO CON PLAN' +
                        '</td>' +
                        '<td class="notif-tarjeta"></td>' +
                    '</tr>' +
                    '<tr style="height: 40px;">' +
                        '<td>' +
                            '<img src="../public/images/claro.svg" class="img-logo"/>' +
                        '</td>' +
                        '<td id="claroPrecioN' + sku.id + '" class="pop-cell"> ' +
                        '</td>' +
                        '<td id="claroPrecioOP' + sku.id + '" class="pop-cell"> ' + 
                        '</td>' +
                        '<td class="pop-cell">' +
                            '<img id="claroRipleyCard' + sku.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg">' +
                        '</td>'+
                        '<td id="claroPlan' + sku.id + '" class="pop-cell"></td>' +
                        '<td id="claroPlancard' + sku.id + '"></td>'+ 
                    '</tr>' +
                    '<tr class="row gray">' +
                        '<td>' +
                        '<img src="../public/images/entel.svg" class="img-logo"/>' +
                        '</td>' +
                        '<td id="entelPrecioN' + sku.id + '" class="pop-cell"> </td>' +
                        '<td id="entelPrecioOP' + sku.id + '" class="pop-cell"> </td>' +
                        '<td class="pop-cell">'+
                            '<img id="entelRipleyCard' + sku.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg">' +
                        '</td>'+
                        '<td id="entelPlan' + sku.id + '" class="pop-cell"></td>' +
                        '<td id="entelPlancard' + sku.id + '"></td>'+
                    '</tr>' +
                    '<tr style="height: 40px;">' +
                        '<td>' +
                        '<img src="../public/images/movistar.svg" class="img-logo" />' +
                        '</td>' +
                        '<td id="movistarPrecioN' + sku.id + '" class="pop-cell"> </td>' +
                        '<td id="movistarPrecioOP' + sku.id + '" class="pop-cell"> </td>' +
                        '<td class="pop-cell">'+
                            '<img id="movistarRipleyCard' + sku.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg">' +
                        '</td>'+
                        '<td id="movistarPlan' + sku.id + '" class="pop-cell"></td>' +
                        '<td id="movistarPlancard' + sku.id + '"></td>'+
                    '</tr>' +
                    '<tr class="row gray">' +
                        '<td>' +
                            '<img src="../public/images/wom.svg" class="img-logo" />' +
                        '</td>' +
                        '<td id="womPrecioN' + sku.id + '" class="pop-cell"> </td>' +
                        '<td id="womPrecioOP' + sku.id + '" class="pop-cell"> </td>' +
                        '<td class="pop-cell">'+
                            '<img id="womRipleyCard' + sku.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg">' +
                        '</td>'+
                        '<td id="womPlan' + sku.id + '" class="pop-cell"></td>' +
                        '<td id="womPlancard' + sku.id + '"></td>'+
                    '</tr>' +
                    '<tr style="height: 40px;">' +
                        '<td>' +
                        '<img src="../public/images/liberado.svg" class="img-logo" />' +
                        '</td>' +
                        '<td id="liberadoPrecioN' + sku.id + '" class="pop-cell"> </td>' +
                        '<td id="liberadoPrecioOP' + sku.id + '" class="pop-cell"> </td>' +
                        '<td id="liberadoRipleyCard' + sku.id + '" class="pop-cell"></td>' +
                    '<td></td>' +
                    '<td class="pop-cell"></td>' +
                '</tr>' +
            '</table>' +
        '</div>'+
        '<div id="disclaimer'+ sku.id +'" class="disclaimer" style="visibility: hidden">'+
            'Contratación del plan sujeta a evaluación comercial de cada operador. Valores informados aplican respecto de planes activos durante 12 ó<br/>'+
            '18 meses, según oferta comercial vigente del operador. Cliente podrá tener un máximo de 5 mandatos vigentes en forma simultánea.'+
        '</div>'
    html += '</div>';
    return html;
}

function arraylify(sku) {
    var text = sku;
    var lines = text.replace(/\r\n/g, "\n").split("\n");;
    var array = [];
    for (i in lines) {
        if (lines[i].length > 0) {
            array.push(lines[i]);
        }
    }
    var def = [];
    var temp = [];
    var stringlocation = []
    for (i in array) {
        if (isNaN(array[i].substring(0, array[i].length - 1)) &&
            isNaN(array[i].substring(3, array[i].length))) {
            stringlocation.push(parseInt(i, 10));
        }
    }
    var index = 0
    if (stringlocation.length == 0) { return array; }
    for (i in array) {
        if (i == stringlocation[index]) {
            index++
            if (temp.length > 0) {
                def.push(temp);
                temp = []
                temp.push(array[i])
            } else {
                temp.push(array[i]);
            }
        } else {
            temp.push(array[i])
        }
        if (i == array.length - 1) {
            def.push(temp);
        }
    }
    return def;
};

function search_name(name) {
    for (i in jsons) {
        if (name == jsons[i].nombre) {
            return jsons[i];
        }
    }
    return null;
}

function search_position_by_name (name) {
    for (i in jsons) {
        if (name == jsons[i].nombre) {
            return i;
        }
    }
    return null;
}

function look_up_name (sku) {
    return search_name_by_sku(sku).name
}

function findlength(array) {
    var int = 0;
    for (i in array) {
        for (e in array[i]) {
            if (isNaN(array[i][e].substring(0, array[i][e].length - 1)) &&
                isNaN(array[i][e].substring(3, array[i][e].length))) {
                int++;
            }
        }
    }
    return int;
}

function formatPrice(numero) {
    numero += '';
    numero = parseFloat(numero.replace(/[^0-9\.]/g, ''));
    if (isNaN(numero) || numero === 0)
        return parseFloat(0).toFixed(0);

    numero = '' + numero.toFixed(0);

    var numero_parts = numero.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(numero_parts[0]))
        numero_parts[0] = numero_parts[0].replace(regexp, '$1' + ',' + '$2');

    return numero_parts.join('.');
}

function search_name_by_sku(sku) {
    for (i in cellArray) {
        for (e in cellArray[i]) {
            if (sku == cellArray[i][e]) return cellArray[i][0];
        }
    }
};
function search_last_sku(sku){
    for (i in cellArray) {
        for (e in cellArray[i]) {
            if (sku == cellArray[i][e]) return cellArray[i][cellArray[i].length-1];
        }
    }
}
function search_array(sku){
    for (i in cellArray) {
        for (e in cellArray[i]) {
            if (sku == cellArray[i][e]) return cellArray[i];
        }
    }
}
function isSku (sku) {
    if(sku.replace(/[^0-9\.]/g, '').length > 5){
        return true
    }
    else {
        return false
    }
}